package me.benana.superapi;

import java.util.Arrays;

import me.benana.supermain.mkremins.fanciful.FancyMessage;

/**
 * @author DrBenana and FancyMessage author
 */
public class SuperMessage extends FancyMessage {	
	
	/**
	 * @author FancyMessage author
	 * @param Message to convert to FancyMessage
	 */
	public SuperMessage(String s) {
		super(s);	
	}
	
	/**
	 * @author DrBenana
	 * @param Amount of the rows in every page
	 * @param Messages
	 * @return Array of String that every cell in it is a page.
	 */
	public static String[] sortByPages(int row_amount, String... messages) {
		String[] args = new String[messages.length/row_amount + (messages.length%row_amount != 0 ? 1 : 0)];
		for (int i = 1; i <= args.length; i++) {
			args[i-1]="";
			for (String s : Arrays.copyOfRange(messages, (i-1)*row_amount, i*row_amount > messages.length ? messages.length : i*row_amount)) {
				args[i-1] = args[i-1]+"\n"+s;
			}
			args[i-1] = args[i-1].replaceFirst("\n", "");
		} 
		return args;
	}
	
}

package me.benana.superapi;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

/**
 * 
 * @author NacOJerk
 *
 */
public class SuperNMS {

	public static String getVersion() {
		String version = Bukkit.getServer().getClass().getPackage().getName();
		version = version.split("\\.")[3];
		return version;
	}
	
	public static Class<?> getNMS(String nms) {
		return getClass("net.minecraft.server." + getVersion() + "." + nms);
	}

	public static Class<?> getClass(String name) {
		try {
			return Class.forName(name);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;

		}
	}

	public static Class<?> getOBC(String OBC) {
		return getClass("org.bukkit.craftbukkit." + getVersion() + "." + OBC);
	}

	public static Object asNMSCopy(ItemStack is) {
		Object nms = null;
		try {
			nms = getOBC("inventory.CraftItemStack").getMethod("asNMSCopy",
					ItemStack.class).invoke(null, is);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nms;
	}
	
	public static ItemStack asBukkitCopy(Object nmsItem) {
		Object iso = null;
		try {
			iso = getOBC("inventory.CraftItemStack").getMethod("asBukkitCopy",
					getNMS("ItemStack")).invoke(null, nmsItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ItemStack is = (ItemStack) iso;
		return is;
	}

	public static Object getField(Object o , String field)throws Exception{
		Field f = o.getClass().getField(field);
		f.setAccessible(true);
		Object o2 = f.get(o);
		f.setAccessible(false);
		return o2;
	}

	public static Object getDeclaredField(Object o , String field)throws Exception{
		Field f = o.getClass().getDeclaredField(field);
		f.setAccessible(true);
		Object o2 = f.get(o);
		f.setAccessible(false);
		return o2;
}
}

package me.benana.superapi;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;

/**
 * SuperArea is used to create 'Areas' like 'WorldEdit' but with very simple API.
 * @author DrBenana
 */
public class SuperArea {
	
	private double[] x = new double[2], y = new double[2], z = new double[2];
	private World world;
	
	/**
	 * @author DrBenana
	 * @param World of the area
	 * @param X cord of the first block location of the area.
	 * @param Y cord of the first block location of the area.
	 * @param Z cord of the first block location of the area.
	 * @param X cord of the last (second) block location of the area.
	 * @param Y cord of the last (second) block location of the area.
	 * @param Z cord of the last (second) block location of the area.
	 */
	public SuperArea(World world, double x0, double y0, double z0, double x1, double y1, double z1) {
		this.x[0] = x0;
		this.x[1] = x1;
		this.y[0] = y0;
		this.y[1] = y1;
		this.z[0] = z0;
		this.z[1] = z1;
		this.world = world;
		
	}
	
	/**
	 * @author DrBenana
	 * @param World of the area
	 * @param X cord of the first block location in the area.
	 * @param Y cord of the first block location in the area.
	 * @param Z cord of the first block location in the area.
	 * @param X cord of the last (second) block location in the area.
	 * @param Y cord of the last (second) block location in the area.
	 * @param Z cord of the last (second) block location in the area.
	 */
	public SuperArea(World world, int x0, int y0, int z0, int x1, int y1, int z1) {
		this(world, (double) x0, (double) y0, (double) z0, (double) x1, (double) y1, (double) z1);
	}
	
	/**
	 * @author DrBenana
	 * @param Location of the first block in the area.
	 * @param Location of the last (second) block in the area.
	 */
	public SuperArea(Location loc1, Location loc2) {
		this(loc1.getWorld(), loc1.getX(), loc1.getY(), loc1.getZ(), loc2.getX(), loc2.getY(), loc2.getZ());
	}
	
	/**
	 * It's just copy a SuperArea. 
	 * @author DrBenana
	 * @param SuperArea
	 */
	public SuperArea(SuperArea sa) {
		this(sa.getFirstLocation(), sa.getSecondLocation());
	}

	public void add(double x0, double y0, double z0, double x1, double y1, double z1) {
		this.setFirstLocation(this.getFirstLocation().add(x0, y0, z0));
		this.setSecondLocation(this.getSecondLocation().add(x1, y1, z1));
	}
	
	/**
	 * Sets all the blocks in the area to selected material.
	 * @author DrBenana
	 * @param Material
	 */
	public void setAllBlocks(Material m) {
		for (Location loc : this.getAllLocations()) {
			loc.getBlock().setType(m);
		}
	}
	
	/**
	 * @author DrBenana
	 * @return Location of the first block in the area
	 */
	public Location getFirstLocation() {
		return new Location(this.world, this.x[0], this.y[0], this.z[0]);
	}
	
	/**
	 * @author DrBenana
	 * @return Location of the last (second) block in the area.
	 */
	public Location getSecondLocation() {
		return new Location(this.world, this.x[1], this.y[1], this.z[1]);
	}
	
	/**
	 * @author DrBenana
	 * @param Location of the first block in the area.
	 */
	public void setFirstLocation(Location loc) {
		this.x[0] = loc.getX();
		this.y[0] = loc.getY();
		this.z[0] = loc.getZ();
		this.world = loc.getWorld();
	}
	
	/**
	 * @author DrBenana
	 * @param Location of the last (second) block in the area.
	 */
	public void setSecondLocation(Location loc) {
		this.x[1] = loc.getX();
		this.y[1] = loc.getY();
		this.z[1] = loc.getZ();
	}
	
	/**
	 * Checks if the location is found in the area.
	 * @author DrBenana
	 * @param Location
	 * @return True when the location is found in the area and False when the location is not found in the area 
	 */
	public boolean isLocationInArea(Location loc) {
		if (!loc.getWorld().getName().equals(this.getWorld().getName())) return false;
		if (isContains(this.x[0], this.x[1], loc.getX()) &&
				isContains(this.y[0], this.y[1], loc.getY()) &&
				isContains(this.z[0], this.z[1], loc.getZ())) 
			return true;
		
		return false;
	} 
	
	/**
	 * @author DrBenana
	 * @return List of the location for every block in the area (Includes air)
	 */
	public List<Location> getAllLocations() {
		List<Location> loc = new ArrayList<>();
		int x1 = (int) (this.x[0]<this.x[1] ? this.x[0] : this.x[1]);
		int x2 = (int) (this.x[0]>this.x[1] ? this.x[0] : this.x[1]);
		int y1 = (int) (this.y[0]<this.y[1] ? this.y[0] : this.y[1]);
		int y2 = (int) (this.y[0]>this.y[1] ? this.y[0] : this.y[1]);
		int z1 = (int) (this.z[0]<this.z[1] ? this.z[0] : this.z[1]);
		int z2 = (int) (this.z[0]>this.z[1] ? this.z[0] : this.z[1]);
        for(int x = x1; x <= x2; x++)
        {
            for(int z = z1; z <= z2; z++)
            {
                for(int y = y1; y <= y2; y++)
                {
                    loc.add(new Location(this.world, x, y, z));
                }
            }
        }
		return loc;
	}
	
	/**
	 * @author DrBenana
	 * @return The world of the area.
	 */
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * @author DrBenana
	 * @return Amount of all the blocks in the area (Includes air)
	 */
	public long countBlocks() {
		int x = ((int) (this.x[0]<this.x[1] ? this.x[1]-this.x[0] : this.x[0]-this.x[1])) + 1,
		y = ((int) (this.y[0]<this.y[1] ? this.y[1]-this.y[0] : this.y[0]-this.y[1])) + 1,
		z = ((int) (this.z[0]<this.z[1] ? this.z[1]-this.z[0] : this.z[0]-this.z[1])) + 1;
		return x*y*z;
	}
	
	public void setToSuperConfig(String path, SuperConfig config) {
		config.set(path + ".world", this.getWorld().getName());
		config.set(path + ".loc1.X", this.getFirstLocation().getBlockX());
		config.set(path + ".loc1.Y", this.getFirstLocation().getBlockY());
		config.set(path + ".loc1.Z", this.getFirstLocation().getBlockZ());
		config.set(path + ".loc2.X", this.getSecondLocation().getBlockX());
		config.set(path + ".loc2.Y", this.getSecondLocation().getBlockY());
		config.set(path + ".loc2.Z", this.getSecondLocation().getBlockZ());
		config.saveConfig();
	}
	
	public static SuperArea fromSuperConfig(String path, SuperConfig config) {
		return new SuperArea(Bukkit.getWorld(config.getString(path + ".world")),
				config.getInt(path + "loc1.X"), config.getInt(path + "loc1.Y"), config.getInt(path + "loc1.Z"),
				config.getInt(path + "loc2.X"), config.getInt(path + "loc2.Y"), config.getInt(path + "loc2.Z"));
	}
	
	private static boolean isContains(double n1, double n2, double a) {
		if (a > n1 && a < n2) {
			return true;
		}
		else if (a < n1 && a > n2) {
			return true;
		}
		else {
			return false;
		}
	}
	
}

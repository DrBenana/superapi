package me.benana.superapi;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * 
 * It's used to create nice inventories (GUIs) easily.
 * @author DrBenana
 *
 */
public class SuperGUI {
	private String name;
	private int maxLines;
	private Inventory lastline;
	
	/**
	 * @author DrBenana
	 * @param Name for the GUI
	 * @param Maximum lines in the GUI
	 */
	public SuperGUI(String name, int maxLines) {
		this.name = name;
		this.maxLines = maxLines > 5 ? 5 : (maxLines < 1 ? 1 : maxLines);
	}
	
	/**
	 * @author DrBenana
	 * @param The page of the inventory
	 * @param ItemStacks/SuperItems that will be found in the inventory
	 * @return GUI as a inventory
	 */
	public Inventory createNewGUI(int page, ItemStack... is) {
		Inventory inv = Bukkit.createInventory(null, (maxLines+1)*9, this.name);
		int i = 0;
		for (; i < is.length; i++) {
			if (i < maxLines*(page-1)*9) continue;
			if (i == maxLines*page*9) {
				for (int f = maxLines*9; f < maxLines*9+9; f++) {
					inv.setItem(f, this.lastline.getContents()[f-maxLines*9]);
				}
				return inv;
			}
			inv.setItem(i, is[i]);
		}
		for (int f = maxLines*9; f < maxLines*9+9; f++) {
			inv.setItem(f, this.lastline.getContents()[f-maxLines*9]);
		}
		return inv;
	}
	
	/**
	 * @author DrBenana
	 * @param Items that will be found in the default last line [UNTIL 9 ITEMS]
	 */
	public void setDefaultLastLine(ItemStack... is) {
		Inventory inv = Bukkit.createInventory(null, 9, this.name+"_last");
		for (int i = 0; i < is.length; i++) {
			inv.setItem(i, is[i]);
		}
		this.lastline = inv;
	}
	
	/**
	 * @author DrBenana
	 * @param MaxLines (int)
	 */
	public void setMaxLines(int maxLines) {
		this.maxLines = maxLines > 6 ? 6 : (maxLines < 1 ? 1 : maxLines);
	}
	
	/**
	 * @author DrBenana
	 * @return MaxLines (int)
	 */
	public int getMaxLines() {
		return this.maxLines;
	}
	
	/**
	 * @author DrBenana
	 * @return Name of the GUI
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @author DrBenana
	 * @param Another GUI
	 * @return IsEquals
	 */
	public boolean equals(Inventory obj) {
		return obj.getName().equals(this.getName());
	}
}

package me.benana.superapi;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * @author DrBenana
 */
public class SuperEvent extends Event {
	private static final HandlerList handlers = new HandlerList();	
	
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
	
    /**
     * Runs every listener that contains this event.
     * @author DrBenana
     */
    public void callEvent() {
    	Bukkit.getPluginManager().callEvent(this);
    }
    
}

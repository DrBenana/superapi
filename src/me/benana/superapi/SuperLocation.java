package me.benana.superapi;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * 
 * SuperLocation is came to replace the regular Location of BukkitAPI.
 * @author DrBenana
 *
 */
public class SuperLocation extends Location {

	/**
	 * @author DrBenana
	 * @param World
	 * @param X [double]
	 * @param Y [double]
	 * @param Z [double]
	 * @param Yaw [float]
	 * @param Pitch [float]
	 */
	public SuperLocation(World world, double x, double y, double z, float yaw, float pitch) {
		super(world, x, y, z, yaw, pitch);
	}
	
	/**
	 * @author DrBenana
	 * @param World
	 * @param X [double]
	 * @param Y [double]
	 * @param Z [double]
	 */
	public SuperLocation(World world, double x, double y, double z) {
		super(world, x, y, z);
	}
	
	/**
	 * Creating a SuperLocation that is found between the first location and the second location.
	 * @author DrBenana
	 * @param Location1
	 * @param Location2
	 */
	public SuperLocation(Location loc1, Location loc2) {
		super(loc1.getWorld(), (loc1.getX() + loc2.getX())/2, (loc1.getY() + loc2.getY())/2, (loc1.getZ() + loc2.getZ())/2);
	}
	
	
	
}

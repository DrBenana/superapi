package me.benana.superapi;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.benana.supermain.SimpleCommand;

/**
 * SuperPlugin can be used instead of the JavaPlugin of BukkitAPI. SuperPlugin doesn't require to write commands in the plugins.yml!
 * @author DrBenana
 */
public class SuperPlugin extends JavaPlugin {	
	
	public boolean superOnCommand(CommandSender sender, String cmd, String[] args) { return false; };
	
	/**
	 * registerEvent is used to register events in the "short way".
	 * @author DrBenana
	 * @param Listener that contains the event
	 */
	public void registerEvent(Listener l) {
		this.getServer().getPluginManager().registerEvents(l, this);
	}
	
	/**
	 * <b> You don't have to register your commands. </b> You can register your commands if you want to add them to the help and/or to allow to call them by methods like "performCommand".
	 * @author DrBenana
	 * @param Command to add
	 * @param Description to the command
	 * @param Usage of the command
	 * @param Permission of the command (You can enter an empty string if you don't have a permission for this command).
	 */
	public void registerCommand(String command, String description, String usage, String permission){
		Object cs = SuperNMS.getOBC("CraftServer").cast(getServer());		
		CommandMap cm;
		try {
			cm = (CommandMap) (cs.getClass().getMethod("getCommandMap").invoke(cs));
			cm.register(command, new SimpleCommand(command, permission, description, usage));
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	@Deprecated
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) { return super.onCommand(sender, command, label, args); }
}
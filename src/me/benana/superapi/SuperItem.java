package me.benana.superapi;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import me.benana.supermain.Glow;

/**
 * SuperItem comes instead of the regular ItemStack in Minecraft. SuperItem contains lot of features
 * like setName and setLore very easily. SuperItem extends from ItemStack so you don't need to be worry about
 * any function is found in the regular ItemStack.
 * @author DrBenana
 */
public class SuperItem extends ItemStack {
	
	/**
	 * Converts an ItemStack to SuperItem.
	 * @author DrBenana
	 * @param ItemStack
	 */
	public SuperItem(ItemStack is) {
		super (is);
	}
	
	/**
	 * @author DrBenana
	 * @param Material of the SuperItem
	 */
	public SuperItem(Material type) {
		super (type);
	}
	
	/**
	 * @author DrBenana
	 * @param Material of the SuperItem
	 * @param Amount from the material
	 */
	public SuperItem(Material type, int amount) {
		super (type, amount);
	}
	
	/**
	 * @author DrBenana
	 * @param Material of the SuperItem
	 * @param Amount from the material
	 * @param Damage (It can be used like data).
	 */
	public SuperItem(Material type, int amount, int damage) {
		super (type, amount, (short) damage);
	}
	
	/**
	 * @author DrBenana
	 * @param Material of the SuperItem
	 * @param Amount from the material
	 * @param Damage (It can be used like data).
	 * @param DisplayName of the item.
	 */
	public SuperItem(Material type, int amount, int damage, String name) {
		super (type, amount, (short) damage);
		ItemMeta im = this.getItemMeta();
		im.setDisplayName(name.replaceAll("&", "§"));
		this.setItemMeta(im);
	}
	
	/**
	 * @author DrBenana
	 * @param Material of the SuperItem
	 * @param Amount from the material
	 * @param Damage (It can be used like data).
	 * @param DisplayName of the item.
	 * @param Lore for the item. For more than one lore you can use '/n'.
	 */
	public SuperItem(Material type, int amount, int damage, String name, String lore) {
		super (type, amount, (short) damage);
		ItemMeta im = this.getItemMeta();
		List<String> lores = new ArrayList<>();
		lores.add(lore.replaceAll("&", "§"));
		im.setDisplayName(name.replaceAll("&", "§"));
		im.setLore(lores);
		this.setItemMeta(im);
	}
	
	/**
	 * @author DrBenana
	 * @param True to add glow and False for remove glow.
	 * @return SuperItem for fast using.
	 */
	public SuperItem setGlow(boolean set) {
		if (set) {
			Glow glow = new Glow(101);
			ItemMeta sim = this.getItemMeta();
			sim.addEnchant(glow, 1, true);
			this.setItemMeta(sim);
		} else {
			Glow glow = new Glow(101);
			ItemMeta sim = this.getItemMeta();
			sim.removeEnchant(glow);
			this.setItemMeta(sim);
		}
		
		return this;
		
	}
	
	@Override
	public void addEnchantment(Enchantment ench, int level) {
		ItemMeta sim = this.getItemMeta();
		sim.addEnchant(ench, level, true);
		this.setItemMeta(sim);
	}
	
	@Override
	public int removeEnchantment(Enchantment ench) {
		ItemMeta im = this.getItemMeta();
		im.removeEnchant(ench);
		this.setItemMeta(im);
		return this.getEnchantmentLevel(ench);
	}
	
	/**
	 * @author DrBenana
	 * @return Name of the SueprItem
	 */
	public String getName() {
		if (this.getItemMeta().hasDisplayName()) {
			return this.getItemMeta().getDisplayName();
		} else {
			return this.getType().name();
		}
	}
	
	/**
	 * @author DrBenana
	 * @return Lore of the SueprItem
	 */
	public String getLore() {
		if (this.getItemMeta().hasLore()) {
			return this.getItemMeta().getLore().get(0);
		} else {
			return "";
		}
	}
	
	/**
	 * @author DrBenana
	 * @param Name for the SuperItem
	 */
	public void setName(String name) {
		ItemMeta im = this.getItemMeta();
		im.setDisplayName(name);
		this.setItemMeta(im);
	}
	
	/**
	 * @author DrBenana
	 * @param Lore for the SuperItem
	 */
	public void setLore(String lore) {
		ItemMeta im = this.getItemMeta();
		List<String> l = new ArrayList<>();
		l.add(lore);
		im.setLore(l);
		this.setItemMeta(im);
	}	
	
	/**
	 * @author DrBenana
	 * @param Name of the player.
	 * @return Skull as an object of SuperItem.
	 */
	public static SuperItem getSkull(String owner) {
	     SuperItem skull = new SuperItem(Material.SKULL_ITEM, 1, 3);
	     SkullMeta skullm = (SkullMeta) skull.getItemMeta();
	     skullm.setOwner(owner);
	     skull.setItemMeta(skullm);
	     return skull;
	}
	
	/**
	 * @author DrBenana
	 * @param Name of the player.
	 * @param DisplayName for the skull. (It doesn't change the skin of the skull)
	 * @return Skull as an object of SuperItem.
	 */
	public static SuperItem getSkull(String owner, String name) {
	     SuperItem skull = new SuperItem(Material.SKULL_ITEM, 1, 3, name);
	     SkullMeta skullm = (SkullMeta) skull.getItemMeta();
	     skullm.setOwner(owner);
	     skull.setItemMeta(skullm);
	     return skull;
	}
	
	/**
	 * @author DrBenana
	 * @param Name of the player.
	 * @param DisplayName for the skull. (It doesn't change the skin of the skull)
	 * @param Lore for the skull.
	 * @return Skull as an object of SuperItem.
	 */
	public static SuperItem getSkull(String owner, String name, String lore) {
	     SuperItem skull = new SuperItem(Material.SKULL_ITEM, 1, 3, name, lore);
	     SkullMeta skullm = (SkullMeta) skull.getItemMeta();
	     skullm.setOwner(owner);
	     skull.setItemMeta(skullm);
	     return skull;
	}
	
	
}

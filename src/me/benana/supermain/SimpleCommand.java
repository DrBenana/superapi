package me.benana.supermain;

import java.util.ArrayList;

import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class SimpleCommand extends BukkitCommand {
	
    public SimpleCommand(String name, String permission, String description, String usage) {
        super(name);
        this.description = description;
        this.usageMessage = usage;
        this.setPermission(permission);
        this.setAliases(new ArrayList<String>());
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
       ServerListener.plugins.forEach(p -> p.superOnCommand(sender, alias, args));
       return false;
    }

}

package me.benana.supermain;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;

import me.benana.superapi.SuperPlugin;

public class ServerListener implements Listener {
	public static ArrayList<SuperPlugin> plugins = new ArrayList<>();
	
	public static void runFirst() {
		for (Plugin pl : Bukkit.getPluginManager().getPlugins()) {
			try {
				if (SuperPlugin.class.isAssignableFrom(Class.forName(pl.getDescription().getMain()))) {
					ServerListener.addMe((SuperPlugin) pl);
				} 
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	@EventHandler
	public void commandsServerEvent(ServerCommandEvent e) {
		for (SuperPlugin pl : plugins) {
			if (pl.superOnCommand(e.getSender(), e.getCommand().split(" ")[0], e.getCommand().replaceFirst(e.getCommand().split(" ")[0] + " ", "").split(" "))) e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void commandsPlayerEvent(PlayerCommandPreprocessEvent e) {
		for (SuperPlugin pl : plugins) {
			if (pl.superOnCommand(e.getPlayer(), e.getMessage().split(" ")[0].replaceFirst("/", ""), e.getMessage().replaceFirst(e.getMessage().split(" ")[0] + " ", "").split(" "))) e.setCancelled(true);
		}
	}
	
	
	private static void addMe(SuperPlugin pl) {
		plugins.add(pl);
	}

}

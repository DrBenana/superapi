package me.benana.supermain;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new ServerListener(), this);
	}
	
	@Override
	public void onLoad() {
		ServerListener.runFirst();
	}
	
}
